package booking.hotels;

import booking.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class TestHotelSearch extends BaseTest {

    @Test
    public void testHotelSearch() {
        driver.get("http://138.68.69.185:3333/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        // Login
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(text(), 'Login')]"))).click();

        By usernameLocator = By.xpath("//input[@placeholder='Enter your email']");
        By passwordLocator = By.xpath("//input[@placeholder='Enter your password']");

        wait.until(ExpectedConditions.elementToBeClickable(usernameLocator));
        driver.findElement(usernameLocator).sendKeys("gbr1@somemail.com");
        driver.findElement(passwordLocator).sendKeys("123");
        driver.findElement(By.xpath("//button[contains(text(),'Login')]")).click();

        // Search hotel
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Where are you going?']")))
                .sendKeys("Traian");
        driver.findElement(By.name("search")).click();

        // verify results
        WebElement hotel = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Traian')]")));
        Assert.assertTrue("Hotel Traian nu a fost afisat", hotel.isDisplayed());

        driver.quit();
    }

}
