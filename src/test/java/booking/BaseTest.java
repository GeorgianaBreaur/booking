package booking;

import org.apache.commons.lang3.SystemUtils;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    public WebDriver driver;

    @Before
    public void setUpTest() {
        // for selenium dependency 4.5 and lower, update the drivers with current version

        driver = new ChromeDriver();
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
    }
}
